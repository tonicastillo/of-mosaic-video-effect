#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){
    ofSetFrameRate(24);
    camWidth 		= 32;	// try to grab at this size.
	camHeight 		= 24;
	
	vidGrabber.setVerbose(true);
	vidGrabber.initGrabber(camWidth,camHeight);
    //videoTexture.allocate(camWidth,camHeight, GL_RGB);
    
    int iPatrones=8;
    for(int i=0;i<iPatrones; i++){
        ofImage patron;
        vPatrones.push_back(patron);
        vPatrones[i].loadImage("patrones/"+ofToString(i)+".png");
    }
}

//--------------------------------------------------------------
void testApp::update(){
    ofBackground(100,100,100);
	
	vidGrabber.update();
	
    if (vidGrabber.isFrameNew()){
        vTonos.clear();
		int totalPixels = camWidth*camHeight*3;
		unsigned char * pixels = vidGrabber.getPixels();
        int iAmpliacion=1;
        
		for (int i = 0; i < totalPixels; i+=3){
            int valor=floor(8*pixels[i]/256);
            vTonos.push_back(7-valor);
        }
	}

    
    iModo=0;
}

//--------------------------------------------------------------
void testApp::draw(){
    ofSetHexColor(0xffffff);
	vidGrabber.draw(20,20,camWidth*4,camHeight*4);
	if(vTonos.size()>0){
        int iLinea=-1;
        for(int i=0;i<vTonos.size(); i++){
            if( i*1.0/(camWidth*1.0)-floor(i*1.0/(camWidth*1.0))==0 ){
                iLinea++;
            }
            vPatrones[vTonos[i]].draw(140+i*31-iLinea*31*camWidth, 20+iLinea*31);
        }
    }

}

//--------------------------------------------------------------
void testApp::keyPressed(int key){

}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){ 

}